#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from functools import reduce as reduce_ft

## Installed Packages
import pyspark.sql.functions as F

def create_record_id(df):
    '''
    Create identifier for PySpark DataFrame by concatenating all fields and computing MD5 hash
           Parameters:
                   df (pyspark.sql.DataFrame): Input dataset
           
           Returns:
                   df (pyspark.sql.DataFrame): Input dataset with record ID
    '''
    all_field_concat = reduce_ft(lambda a, x: F.md5(F.concat(F.when(F.isnull(a), '').otherwise(a), 
                                                             F.when(F.isnull(x), '').otherwise(x)))
                                 , map(F.col, df.columns))
    df = df.withColumn("RID", all_field_concat)
    return df


def create_join_attribute(df, join_columns, record_data):
    '''
    Create join attribute for PySpark DataFrame by concatenating and trimming specified fields
           Parameters:
                   df (pyspark.sql.DataFrame): Input dataset
                   join_columns (list): List of column names to use for set-similarity join
                   record_data (str): Comma-separated string of column indices to use for set-similarity join (1-based)
           
           Returns:
                   df (pyspark.sql.DataFrame): Input dataset with join attribute
    '''
    if join_columns in [None, []]:
        join_columns = [df.columns[x-1] for x in map(int, record_data.split(","))]
    field_concat = reduce_ft(lambda a, x: F.trim(F.concat(F.when(F.isnull(a), '').otherwise(a), 
                                                          F.lit(' '),
                                                          F.when(F.isnull(x), '').otherwise(x)))
                             , map(F.col, join_columns))
    df = df.withColumn('join_attribute', field_concat)
    return df