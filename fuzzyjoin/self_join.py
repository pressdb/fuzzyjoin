#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pandas as pd


## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.token_ordering.basic_token_ordering as bto
import fuzzyjoin.kernel.basic_kernel as bk
import fuzzyjoin.helpers as H

def self_join(spark, df_filepath, join_columns, sep, 
                     sim_threshold, record_data, 
                     schema, record_id, **kwargs):
    '''
    PySpark-based implementation of set-similarity self-join
           Parameters:
                   spark (pyspark.sql.SparkSession): SparkSession
                   df_filepath (str): Filepath to input dataset
                   join_columns (list): List of column names to use for set-similarity join
                   sep (str): File delimiter for dataset
                   schema (str): SQL-like schema for dataset
                   record_data (str): Comma-separated string of column indices to use for set-similarity join (1-based)
                   record_id (str): Name of data field to use as record identifier
                   sim_threshold (float): Similarity threshold for Jaccard index metric
                   **kwargs: Additional arguments for spark.read.csv
           
           Returns:
                   df (pyspark.sql.DataFrame): Dataset with ids and similarity scores if there are matches, None otherwise
    '''
    
    # Load Dataset
    df = spark.read.csv(path=df_filepath, sep=sep, schema=schema, **kwargs)
    
    # Add record ID
    df = H.create_record_id(df=df)
    
    # Setup join attribute
    if join_columns in [None, []]:
        join_columns = [df.columns[x-1] for x in map(int, record_data.split(","))]
    df = H.create_join_attribute(df=df, join_columns=join_columns, record_data=record_data)
    
    # Basic Token Ordering
    token_ordering = bto.bto(df=df, join_attribute='join_attribute')
    
    token_ordering_list = token_ordering.collect()
    
    # Basic Kernel
    if record_id in [None, [], ""]:
        record_id = "RID"
    similar_pairs = bk.basic_kernel(df=df, RID_name=record_id, 
                                 join_attribute_name='join_attribute', 
                                 token_ordering=token_ordering_list, sim_threshold=sim_threshold)
    
    # Save Output
    if not similar_pairs.isEmpty():
        df_similar_pairs = spark.createDataFrame(similar_pairs, ["RID1", "RID2", "JaccardSimilarity"])
        return df_similar_pairs
    return None

