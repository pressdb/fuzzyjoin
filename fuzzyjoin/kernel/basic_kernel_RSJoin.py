#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from operator import add


## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.filtering.reduction_filtering as rf
import fuzzyjoin.kernel.helpers as H

def basic_kernel_map_RSJoin(record, RID_name, join_attribute_name, token_ordering, relation=0):
    '''
    Map step of Basic Kernel stage
           Parameters:
                   record (pyspark.sql.Row): Row from input dataset
                   RID_name (str): Name of data field to use as record identifier
                   join_attribute_name (str): Name of data field to use for set-similarity join
                   token_ordering (list): Global token ordering
                   relation (int): binary indicator for dataset (0 = dataset R; 1 = dataset S)
           
           Returns:
                   output_pairs (RDD): Pairs of individual tokens with corresponding (relation, record id, join attribute) tuples
    '''
    
    # retrieves the original records one by one, and extracts the RID and the join-attribute value for each record.
    rid = record[RID_name]
    join_attribute = record[join_attribute_name]
    
    # It tokenizes the join attribute and reorders the tokens based on their frequencies.
    join_attribute_tokenized = tp.tokenization(dc.string_cleaning(join_attribute))
    
    ## Discard tokens not in token ordering from smaller dataset
    join_attribute_tokenized = [x for x in join_attribute_tokenized if x in token_ordering]
    join_attribute_token_global_order = sorted(join_attribute_tokenized, 
                                               key=lambda x: token_ordering.index(x))
    
    # Next, the function computes the prefix length and extracts the prefix tokens.
    prefix_length = rf.jaccard_prefix_length(len(join_attribute_token_global_order))
    prefix_tokens = join_attribute_token_global_order[0:prefix_length]
    
    # Finally, the function uses either the individual tokens or the grouped tokens routing strategy 
    # to generate the output pairs.
    # (Using the individual tokens strategy here)
    output_pairs = []
    for token in prefix_tokens:
        output_pairs.append(((token), (relation, rid, join_attribute)))
    return output_pairs


def basic_kernel_reduce_RSJoin(key_values, token_ordering, sim_threshold=0.5):
    '''
    Reduce step of Basic Kernel stage
           Parameters:
                   key_values (str): (RDD): Pairs of individual tokens with corresponding (record id, join attribute) tuples
                   token_ordering (list): Global token ordering
                   sim_threshold (float): Similarity threshold for Jaccard index metric
           
           Returns:
                   output_pairs (RDD): RID Pairs with similarity score at or above threshold
    '''
    
    
    # for each pair of record projections, the reducer applies the additional filters 
    # (e.g., length filter, positional filter, and suffix filter) and verifies the pair if it survives. 
    
    # Verify pairs from second relation against first relation
    
    token = key_values[0]
    values = key_values[1]
    output_pairs = []
    for i, value1 in enumerate(values):
        (relation1, rid1, join_attribute1_str) = value1
        for j, value2 in enumerate(values):
            (relation2, rid2, join_attribute2_str) = value2
            if j > i:
                if rid1 != rid2 and relation1 != relation2 and relation1 == 0:
                    join_attribute1 = tp.tokenization(dc.string_cleaning(join_attribute1_str))
                    join_attribute2 = tp.tokenization(dc.string_cleaning(join_attribute2_str))
                    if H.pass_filters(join_attribute1, join_attribute2, token, token_ordering=token_ordering):
                        sim_score = sim.jaccard_index(join_attribute1, join_attribute2)
                        # If a pair passes the similarity threshold, the reducer outputs RID pairs and their similarity values.
                        if sim_score >= sim_threshold:
                            output_pairs.append((rid1, rid2, sim_score))
    return output_pairs


def basic_kernel_RSJoin(df_R, RID_name_R, join_attribute_name_R, 
                        df_S, RID_name_S, join_attribute_name_S, 
                        token_ordering, sim_threshold=0.5):
    '''
    PySpark-based implementation of Basic Kernel stage of set-similarity join
           Parameters:
                   df_R (pyspark.sql.DataFrame): Input dataset R
                   RID_name_R (str): Name of data field in R to use as record identifier
                   join_attribute_name_R (str): Name of data field in R to use for set-similarity join
                   df_S (pyspark.sql.DataFrame): Input dataset S
                   RID_name_S (str): Name of data field in S to use as record identifier
                   join_attribute_name_S (str): Name of data field in S to use for set-similarity join
                   token_ordering (list): Global token ordering
                   sim_threshold (float): Similarity threshold for Jaccard index metric
           
           Returns:
                   similar_pairs (RDD): RID Pairs with similarity score at or above threshold
    '''
    # Map
    basic_kernel_single_tokens_R = df_R.rdd.flatMap(lambda x: basic_kernel_map_RSJoin(x, RID_name=RID_name_R, 
                                                                       join_attribute_name=join_attribute_name_R, 
                                                                       token_ordering=token_ordering,
                                                                             relation=0))
    
    basic_kernel_single_tokens_R = basic_kernel_single_tokens_R.groupByKey().mapValues(list)
    
    basic_kernel_single_tokens_S = df_S.rdd.flatMap(lambda x: basic_kernel_map_RSJoin(x, RID_name=RID_name_S, 
                                                                       join_attribute_name=join_attribute_name_S, 
                                                                       token_ordering=token_ordering,
                                                                             relation=1))
    
    basic_kernel_single_tokens_S = basic_kernel_single_tokens_S.groupByKey().mapValues(list)
        
    # Reduce
    basic_kernel_single_tokens_R_S = basic_kernel_single_tokens_R.union(basic_kernel_single_tokens_S)
    
    # Only save single tokens that have more than one (RID, join_attribute) pair associated with them
    basic_kernel_single_tokens_R_S = basic_kernel_single_tokens_R_S.reduceByKey(add).filter(lambda x: len(x[1]) > 1)
    
    similar_pairs = basic_kernel_single_tokens_R_S.map(lambda x: 
                                                       basic_kernel_reduce_RSJoin(x,
                                                                                  token_ordering=token_ordering,
                                                                                  sim_threshold=sim_threshold)).filter(lambda x: x != []).flatMap(lambda x: x).distinct()
        
    return similar_pairs