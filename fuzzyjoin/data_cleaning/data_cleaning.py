#!/usr/bin/env python
# coding: utf-8

# Import Packages

## Standard Packages
import re

def remove_punctuation(A):
    '''
    Removes punctuation (all non-word characters) from string
           Parameters:
                   A (str): Input string
           
           Returns:
                   A (str): Input string with any punctuation characters removed
    '''
    A = re.sub(pattern=r"[^\w]", repl=" ", string=A) 
    return A
# TODO: Note that this would remove tokens such as "C++"


def normalize_case(A):
    '''
    Converts string to lower case
           Parameters:
                   A (str): Input string
           
           Returns:
                   A (str): Input string with any upper case characters converted to lower case 
    '''
    A = A.lower()
    return A


def trim(A):
    '''
    Removes leading whitespace, trailing whitespace, or multiple consecutive whitespaces within string
           Parameters:
                   A (str): Input string
           
           Returns:
                   A (str): Input string with any extra whitespaces removed
    '''
    A = re.sub(pattern=r"\s{2,}", repl=" ", string=A)
    A = A.strip()
    return A


def string_cleaning(A):
    '''
    Removes punctuation and extra whitespace from string; converts to all lower case
           Parameters:
                   A (str): Input string
           
           Returns:
                   A (str): Lower case string with any extra whitespaces and punctuation removed
    '''
    A = remove_punctuation(A)
    A = normalize_case(A)
    A = trim(A)
    return A
