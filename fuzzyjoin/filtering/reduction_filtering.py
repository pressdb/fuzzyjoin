#!/usr/bin/env python
# coding: utf-8

# Import Packages

## Standard Packages
import math

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim

# Prefix Filtering
def jaccard_prefix_length(token_count, sim_threshold=0.5):
    '''
    Calculates prefix length of string based on Jaccard index
           Parameters:
                   token_count (int): Number of tokens in string
                   sim_threshold (float): Similarity threshold for Jaccard index metric
           
           Returns:
                   out (int): Prefix length
    '''
    out = token_count - math.ceil(sim_threshold * token_count) + 1
    return out


# Length Filtering
def jaccard_length_filtering(A, B, sim_threshold=0.5):
    '''
    Determines whether given string pair satisfies length filtering threshold
    From https://who.rocq.inria.fr/Vassilis.Christophides/Big/local_copy/Finding_Similar_Items/a15-xiao.pdf
    (Page 15:6)
           Parameters:
                   A (str): Input string A
                   B (str): Input string B
                   sim_threshold (float): Similarity threshold for Jaccard index metric
           
           Returns:
                   (bool): True if pair satisfies condition; false otherwise
    '''
    
    A = set(A)
    B = set(B)
    if sim_threshold * len(A) <= len(B):
        return True
    return False

assert jaccard_length_filtering(A=["I", "will", "call", "back"], B=["I"], sim_threshold=0.5) == False

# Positional Filtering
def get_jaccard_position_filter_lower_bound(n_tokens_A, n_tokens_B, sim_threshold=0.5):
    '''
    Calculates lower bound of positional filter threshold of two strings based on Jaccard index
           Parameters:
                   n_tokens_A (int): Number of tokens in string A
                   n_tokens_B (int): Number of tokens in string B
                   sim_threshold (float): Similarity threshold for Jaccard index metric
           
           Returns:
                   out (int): Lower bound
    '''
    out = math.ceil(sim_threshold * (n_tokens_A + n_tokens_B) / (1 + sim_threshold))
    return out

def jaccard_positional_filtering(A, B, token_stop, sim_threshold=0.5):
    '''
    Determines whether given string pair satisfies positional filtering threshold for given token
    From https://who.rocq.inria.fr/Vassilis.Christophides/Big/local_copy/Finding_Similar_Items/a15-xiao.pdf
    (Page 15:7) and implementation in original fuzzyjoin algorithm
           Parameters:
                   A (list): Input string A, as list of tokens
                   B (list): Input string B, as list of tokens
                   token_stop (str): input token
                   sim_threshold (float): Similarity threshold for Jaccard index metric
           
           Returns:
                   (bool): True if pair satisfies condition; false otherwise
    '''
    
    # position of the last gram in common on A
    position_A = A.index(token_stop)
    
    # position of the last gram in common on B
    position_B = B.index(token_stop)
    
    current_overlap_amount = sim.overlap_similarity(A[0:position_A+1], B[0:position_B+1])
    
    min_unseen_tokens = min(len(A[position_A+1:]), len(B[position_B+1:]))
    
    threshold = get_jaccard_position_filter_lower_bound(len(A), len(B), sim_threshold=sim_threshold)
    if current_overlap_amount + min_unseen_tokens >= threshold:
        return True
    return False

assert jaccard_positional_filtering(A=["B", "C", "D", "E", "F"], 
                                    B=["A", "B", "C", "D", "E"], 
                                    token_stop="B", sim_threshold=0.5) == True