#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Custom Packages
import fuzzyjoin.token_ordering.basic_token_ordering as bto

def bto_RS_Join(df_R, n_R, df_S, n_S, join_R, join_S):
    '''
    PySpark-based implementation of Basic Token Ordering for R-S set-similarity join
           Parameters:
                   df_R (pyspark.sql.DataFrame): Input dataset R
                   n_R (int): Count of records in R
                   df_S (pyspark.sql.DataFrame): Input dataset S
                   n_S (int): Count of records in S
                   join_R (str): Name of data field in R to use for set-similarity join and tokenization
                   join_S (str): Name of data field in S to use for set-similarity join and tokenization
           
           Returns:
                   token_ordering (RDD): Global token ordering
    '''
    # Run Phase 1 on dataset with fewer records
    if n_R <= n_S:
        bto_phase_1 = bto.bto_phase_1(df=df_R, join_attribute=join_R)
    else:
        bto_phase_1 = bto.bto_phase_1(df=df_S, join_attribute=join_S)
        
    ### Discard tokens not in smaller dataset
    token_ordering = bto.bto_phase_2(phase_1_output=bto_phase_1)
    return token_ordering