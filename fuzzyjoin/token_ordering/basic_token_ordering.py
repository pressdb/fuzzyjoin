#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from operator import add

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim

def bto_phase_1(df, join_attribute):
    '''
    Phase 1 of Basic Token Ordering
           Parameters:
                   df (pyspark.sql.DataFrame): Input dataset
                   join_attribute (str): Name of data field to use for set-similarity join and tokenization
           
           Returns:
                   token_reduce (RDD): Pairs of individual tokens with their total counts across dataset
    '''
    # Map
    token_maps = df.select(join_attribute).rdd.flatMap(lambda line: tp.tokenization(dc.string_cleaning(line[0]))).map(lambda token: (token, 1))
    
    # Combine/Reduce
    token_reduce = token_maps.reduceByKey(add)
    
    return token_reduce

def bto_phase_2(phase_1_output):
    '''
    Phase 2 of Basic Token Ordering
           Parameters:
                   phase_1_output (RDD): Pairs of individual tokens with their total counts across dataset
           
           Returns:
                   token_ordering (RDD): Global token ordering
    '''
    # Map
    token_count_swap = phase_1_output.map(lambda x: (x[1], x[0])).sortByKey()
    
    # Combine/Reduce
    token_ordering = token_count_swap.map(lambda x: x[1])
    
    return token_ordering

def bto(df, join_attribute):
    '''
    PySpark-based implementation of Basic Token Ordering for set-similarity join
           Parameters:
                   df (pyspark.sql.DataFrame): Input dataset
                   join_attribute (str): Name of data field to use for set-similarity join and tokenization
           
           Returns:
                   token_ordering (RDD): Global token ordering
    '''
    ## Phase 1
    token_counts = bto_phase_1(df=df, join_attribute=join_attribute)
    
    ## Phase 2
    token_ordering = bto_phase_2(token_counts)
    
    return token_ordering