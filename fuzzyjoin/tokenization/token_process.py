#!/usr/bin/env python
# coding: utf-8

def split_tokens(A, delim=" "):
    '''
    Splits string into tokens based on delimiter
           Parameters:
                   A (str): Input string A
                   delim (str): Specifies how tokens should be split in A
           
           Returns:
                   (list): List containing tokens of input string
    '''
    return A.split(delim)


def add_token_counts(A, sep="_"):
    '''
    Appends each token in a list of tokens with a count of how many times that token has appeared at that point
           Parameters:
                   A (list): Input string A, as list of tokens
                   sep (str): Specifies how tokens should be separated from their counts
           
           Returns:
                   (list): List containing tokens of input string, appended with counts
    '''
    A = sorted(A)
    
    current_token_count = 1
    for i, token in enumerate(A):
        total_token_count = A.count(token)
        A[i] = token+sep+str(current_token_count)
        if total_token_count > 1:
            current_token_count += 1
        else:
            current_token_count = 1
    
    return(A)


def tokenization(A, token_delim=" ", token_sep="_"):
    '''
    Splits string into tokens and appends each with their counts
           Parameters:
                   A (str): Input string A
                   token_delim (str): Specifies how tokens should be split in A
                   token_sep (str): Specifies how tokens should be separated from their counts
           
           Returns:
                  token_count_list (list): List containing tokens of input string, appended with counts
    '''
    token_list = split_tokens(A, delim=token_delim)
    token_count_list = add_token_counts(token_list, sep=token_sep)
    return token_count_list



