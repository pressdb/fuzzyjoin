#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pandas as pd
import pytest

## Custom Packages
import fuzzyjoin.self_join as self_join

# Load expected output for record join
# NOTE: This output was extracted from original fuzzyjoin repo
test_df_similar_pairs = pd.read_csv("data/dblp-small.expected/recordpairs-000/expected.txt", sep=":",
                                   names=["RID1","dblp_id1", "publication_title1", "authors1", "other_info1",
                                         "dblp_id2", "publication_title2", "authors2", "other_info2"])

test_df_similar_pairs[["other_info1","JaccardSimilarity","RID2"]] = test_df_similar_pairs["other_info1"].str.split(";", expand=True)

test_df_similar_pairs = test_df_similar_pairs.sort_values(by=["dblp_id1", "dblp_id2"]).reset_index(drop=True)


def test_self_join(df_filepath='data/dblp-small/raw-000/part-00000', 
                     join_columns=['publication_title','authors'], sep=':', 
                     sim_threshold=0.5, record_data="2,3", 
                     schema='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_id='dblp_id'):

    # Start Spark Session
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()

    df_similar_pairs = self_join(spark=spark, df_filepath=df_filepath, 
                                    join_columns=join_columns, sep=sep,
                                    sim_threshold=sim_threshold, 
                                    record_data=record_data, schema=schema, record_id=record_id)

    assert df_similar_pairs is not None

    # Test Output Pair Matching
    df_similar_pairs = df_similar_pairs.toPandas()

    df_similar_pairs.columns = ["dblp_id1", "dblp_id2","JaccardSimilarity"]
    df_similar_pairs = df_similar_pairs.sort_values(by=["dblp_id1", "dblp_id2"]).reset_index(drop=True)

    assert df_similar_pairs[["dblp_id1", "dblp_id2"]].equals(test_df_similar_pairs[["dblp_id1", "dblp_id2"]])
    
    temp_f = lambda x: round(float(x), 6)
    assert list(df_similar_pairs["JaccardSimilarity"].apply(temp_f)) == list(test_df_similar_pairs["JaccardSimilarity"].apply(temp_f))
    
    # Close the Spark Context
    sc.stop()
