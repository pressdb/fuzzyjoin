#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pandas as pd
import pytest

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.token_ordering.basic_token_ordering as bto
import fuzzyjoin.helpers as H
import fuzzyjoin.kernel.basic_kernel as bk

# Load expected output for record join
# NOTE: This output was extracted from original fuzzyjoin repo
test_df_similar_pairs = pd.read_csv("data/dblp-small.expected/recordpairs-000/expected.txt", sep=":",
                                   names=["RID1","dblp_id1", "publication_title1", "authors1", "other_info1",
                                         "dblp_id2", "publication_title2", "authors2", "other_info2"])

test_df_similar_pairs[["other_info1","JaccardSimilarity","RID2"]] = test_df_similar_pairs["other_info1"].str.split(";", expand=True)

test_df_similar_pairs = test_df_similar_pairs.sort_values(by=["dblp_id1", "dblp_id2"]).reset_index(drop=True)



def test_basic_kernel(df_filepath='data/dblp-small/raw-000/part-00000', 
                     join_columns=['publication_title','authors'], sep=':', 
                     sim_threshold=0.5, record_data="2,3", 
                     schema='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_id='dblp_id'):
    # Start Spark Session
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    
    # NOTE: This dataset was extracted from original fuzzyjoin repo
    
    # Load Dataset
    df = spark.read.csv(path=df_filepath, sep=sep, schema=schema)
    
    # Add record ID
    df = H.create_record_id(df=df)
    
    # Setup join attribute
    if join_columns in [None, []]:
        join_columns = [df.columns[x-1] for x in map(int, record_data.split(","))]
    df = H.create_join_attribute(df=df, join_columns=join_columns, record_data=record_data)
    
    # Basic Token Ordering
    token_ordering = bto.bto(df=df, join_attribute='join_attribute')
    
    token_ordering_list = token_ordering.collect()
    
    # Basic Kernel
    similar_pairs = bk.basic_kernel(df=df, RID_name=record_id, 
                                 join_attribute_name='join_attribute', 
                                 token_ordering=token_ordering_list, sim_threshold=sim_threshold)
    
    
    assert similar_pairs.isEmpty() is False
    
    
    # Record Join
    df_similar_pairs = spark.createDataFrame(similar_pairs, ["RID1", "RID2", "JaccardSimilarity"])
    df_similar_pairs = df_similar_pairs.join(df.alias("df_as1"), df_similar_pairs.RID1 == F.col("df_as1."+record_id))                                    .join(df.alias("df_as2"), df_similar_pairs.RID2 == F.col("df_as2."+record_id))
    df_similar_pairs = df_similar_pairs.toPandas()

    df_similar_pairs.columns = ["RID1","RID2","JaccardSimilarity","dblp_id1","publication_title1",
                            "authors1","other_info1","RID1_copy","join_attribute1","dblp_id2","publication_title2",
                            "authors2","other_info2","RID2_copy","join_attribute2"]
    df_similar_pairs = df_similar_pairs.sort_values(by=["RID1", "RID2"]).reset_index(drop=True)
    
    assert df_similar_pairs[["dblp_id1", "dblp_id2"]].equals(test_df_similar_pairs[["dblp_id1", "dblp_id2"]])
    
    temp_f = lambda x: round(float(x), 6)
    assert list(df_similar_pairs["JaccardSimilarity"].apply(temp_f)) == list(test_df_similar_pairs["JaccardSimilarity"].apply(temp_f))
    
    # Close the Spark Context
    sc.stop()