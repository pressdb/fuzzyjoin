#!/usr/bin/env python
# coding: utf-8

# Import Packages

## Standard Packages
import re

## Installed Packages
import pytest

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp

test_split_tokens_params = [
    ("I will call back", ["I", "will", "call", "back"])
]


@pytest.mark.parametrize(
    'test_data, expected_output',
    test_split_tokens_params
)
def test_split_tokens(test_data, expected_output):
    assert tp.split_tokens(test_data, delim=" ") == expected_output


test_add_token_counts_params = [
    (["I", "will", "call", "back"], ["I_1", "back_1", "call_1", "will_1"]),
    (["Dan", "Schonfeld", "Dan", "Lelescu"], ["Dan_1", "Dan_2", "Lelescu_1", "Schonfeld_1"])
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_add_token_counts_params
)
def test_add_token_counts(test_data, expected_output):
    assert tp.add_token_counts(test_data, sep="_") == expected_output

test_tokenization_params = [
    ("I will call back", ["I_1", "back_1", "call_1", "will_1"])
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_tokenization_params
)
def test_tokenization(test_data, expected_output):
    assert tp.tokenization(test_data, token_delim=" ", token_sep="_") == expected_output
