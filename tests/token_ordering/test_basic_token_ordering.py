#!/usr/bin/env python
# coding: utf-8

# Import Packages
## Standard Packages
from functools import reduce as reduce_ft

## Installed Packages
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext, Window
import pyspark.sql.functions as F
import pytest

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim
import fuzzyjoin.token_ordering.basic_token_ordering as bto
import fuzzyjoin.helpers as H


# Load expected output of token ordering
# NOTE: This output was extracted from original fuzzyjoin repo
test_token_ordering = []
with open('data/dblp-small.expected/tokens-000/expected.txt', 'r') as f:
    for line in f:
        test_token_ordering.append(line.strip())

def test_bto(df_filepath='data/dblp-small/raw-000/part-00000', 
             join_columns=['publication_title','authors'], sep=':', record_data="2,3",
            schema='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING'):
    # Start Spark Session
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    
    # NOTE: This dataset was extracted from original fuzzyjoin repo
    
    # Load Dataset
    df = spark.read.csv(path=df_filepath, sep=sep, schema=schema)
    
    # Add record ID
    df = H.create_record_id(df=df)
    
    # Setup join attribute
    if join_columns in [None, []]:
        join_columns = [df.columns[x-1] for x in map(int, record_data.split(","))]
    df = H.create_join_attribute(df=df, join_columns=join_columns, record_data=record_data)
    
    # Basic Token Ordering
    token_ordering = bto.bto(df=df, join_attribute='join_attribute')
    
    token_ordering_list = token_ordering.collect()
    
    assert set(test_token_ordering) == set(token_ordering_list)
    
    # Close the Spark Context
    sc.stop()
