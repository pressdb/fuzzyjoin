#!/usr/bin/env python
# coding: utf-8

# Import Packages


## Installed Packages
import pytest

## Custom Packages
import fuzzyjoin.tokenization.token_process as tp
import fuzzyjoin.data_cleaning.data_cleaning as dc
import fuzzyjoin.similarity_scores.similarity_scores as sim

test_clean_token_jaccard_params = [
    ("VORTEX  Video Retrieval and Tracking from Compressed Multimedia Databases. Dan Schonfeld Dan Lelescu", "VORTEX  Video Retrieval and Tracking from Compressed Multimedia Databases ¾ Visual Search Engine. Dan Schonfeld Dan Lelescu", 0.7647059)
]

@pytest.mark.parametrize(
    'test_A, test_B, expected_output',
    test_clean_token_jaccard_params
)
def test_clean_token_jaccard(test_A, test_B, expected_output):
    test_A = tp.tokenization(dc.string_cleaning(test_A))
    test_B = tp.tokenization(dc.string_cleaning(test_B))
    
    assert round(sim.jaccard_index(test_A, test_B), 7) == expected_output