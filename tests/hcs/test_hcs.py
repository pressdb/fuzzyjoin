#!/usr/bin/env python
# coding: utf-8

# Import Packages

## Installed Packages
import networkx as nx
import pytest

## Custom Packages
import fuzzyjoin.hcs.hcs as hcs

G = nx.Graph()
G.add_edges_from([(1,2),(2,3),(3,4),(4,5),(5,1),(1,4),(2,4),(3,5),
                       (2,'y'),('b',3),('a','A'),('A','B'),('B','b'),('A','b'),('B','a'),
                      ('x','y'),('y','z'),('x','z'),('z','a'),('a','b'),('b','y')
                       ])

# Multiple possibilities for HCS due to multiple possible minimum edge cuts
# (in turn due to multiple possible dominating sets)
# TODO: Add heuristic improvements from HCS paper
G_subgraphs_A = [nx.Graph(G.subgraph(['x','y','z'])), nx.Graph(G.subgraph(['a','b','A', 'B'])),
                   nx.Graph(G.subgraph([1,2,3,4,5]))]
G_subgraphs_A = nx.algorithms.operators.all.compose_all(G_subgraphs_A)

G_cluster_labels_A = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1,
                         'x': 2, 'y': 2, 'z': 2,
                         'a': 3, 'b': 3, 'A': 3, 'B': 3}


G_subgraphs_B = [nx.Graph(G.subgraph(['x'])), nx.Graph(G.subgraph(['y'])), nx.Graph(G.subgraph(['z'])), 
                   nx.Graph(G.subgraph(['a','b','A', 'B'])),
                   nx.Graph(G.subgraph([1,2,3,4,5]))]

G_subgraphs_B = nx.algorithms.operators.all.compose_all(G_subgraphs_B)

G_cluster_labels_B1 = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1,
                         'x': 5, 'y': 2, 'z': 4,
                         'a': 3, 'b': 3, 'A': 3, 'B': 3}

G_cluster_labels_B2 = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1,
                         'x': 5, 'y': 3, 'z': 4,
                         'a': 2, 'b': 2, 'A': 2, 'B': 2}


G_subgraphs_C = [nx.Graph(G.subgraph(['x'])), nx.Graph(G.subgraph(['y', 'z'])),
                   nx.Graph(G.subgraph(['a','b','A', 'B'])),
                   nx.Graph(G.subgraph([1,2,3,4,5]))]

G_subgraphs_C = nx.algorithms.operators.all.compose_all(G_subgraphs_C)


G_cluster_labels_C = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1,
                         'x': 4, 'y': 2, 'z': 2,
                         'a': 3, 'b': 3, 'A': 3, 'B': 3}


test_hcs_multiple_params = [
    (G, (G_subgraphs_A, G_subgraphs_B, G_subgraphs_C))
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_hcs_multiple_params
)
def test_hcs_multiple(test_data, expected_output):
    assert (nx.is_isomorphic(hcs.HCS(test_data.copy()), expected_output[0]) or 
            nx.is_isomorphic(hcs.HCS(test_data.copy()), expected_output[1]) or
            nx.is_isomorphic(hcs.HCS(test_data.copy()), expected_output[2]))


H = nx.Graph()
H.add_edges_from([(1,2)])

test_hcs_single_params = [
    (H, H)
]

@pytest.mark.parametrize(
    'test_data, expected_output',
    test_hcs_single_params
)
def test_hcs_single(test_data, expected_output):
    assert nx.is_isomorphic(hcs.HCS(test_data.copy()), expected_output)

test_hcs_per_cc_params = [
    (G_subgraphs_A, G_subgraphs_A)
]
@pytest.mark.parametrize(
    'test_data, expected_output',
    test_hcs_per_cc_params
)
def test_hcs_per_cc(test_data, expected_output):
    assert nx.is_isomorphic(hcs.HCS_per_cc(test_data), expected_output)



test_labeled_hcs_per_cc_params = [
    (G, (G_cluster_labels_A, G_cluster_labels_B1, G_cluster_labels_B2, G_cluster_labels_C))
]
@pytest.mark.parametrize(
    'test_data, expected_output',
    test_labeled_hcs_per_cc_params
)
def test_labeled_hcs_per_cc(test_data, expected_output):
    assert (hcs.labelled_HCS_per_cc(test_data.copy()) == expected_output[0] or
           hcs.labelled_HCS_per_cc(test_data.copy()) == expected_output[1] or
           hcs.labelled_HCS_per_cc(test_data.copy()) == expected_output[2] or
           hcs.labelled_HCS_per_cc(test_data.copy()) == expected_output[3])
