#!/usr/bin/env python

from setuptools import setup

REQUIRES = []
with open('requirements.txt') as f:
    for line in f:
        line, _, _ = line.partition('#')
        line = line.strip()
        if ';' in line:
            requirement, _, specifier = line.partition(';')
            for_specifier = EXTRAS.setdefault(':{}'.format(specifier), [])
            for_specifier.append(requirement)
        else:
            REQUIRES.append(line)

with open('test-requirements.txt') as f:
    TESTS_REQUIRES = f.readlines()

setup(name='fuzzyjoin',
      version='0.1',
      description='A PySpark-based fork of the set-similarity join algorithm implemented in Efficient Parallel Set-Similarity Joins Using MapReduce. Rares Vernica, Michael J. Carey, Chen Li SIGMOD 2010.',
      author='PressDB',
      author_email='partners@pressdb.ai',
      license="GNU GENERAL PUBLIC LICENSE Version 3",
      url='https://www.pressdb.ai/',
      keywords=["Entity Resolution", "Record Linkage", "Entity Clustering"],
      install_requires=REQUIRES,
      tests_require=TESTS_REQUIRES,
      packages=['fuzzyjoin', 'fuzzyjoin.data_cleaning', 'fuzzyjoin.filtering',
                'fuzzyjoin.hcs', 'fuzzyjoin.kernel', 'fuzzyjoin.similarity_scores',
                'fuzzyjoin.tokenization', 'fuzzyjoin.token_ordering']
      )

