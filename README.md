# fuzzyjoin

A PySpark-based fork of the set-similarity join algorithm implemented in Efficient Parallel Set-Similarity Joins Using MapReduce. Rares Vernica, Michael J. Carey, Chen Li *SIGMOD 2010*.


## Features

This module can help you find similar records within a tabular dataset (a.k.a. *self-join*) or between two tabular datasets (*R-S Join*).

- Can specify one or more attributes on which to execute similarity join.

- Can set threshold for metric used in similarity join (Jaccard metric is only one currently available).

- Written in PySpark to take advantage of modern distributed computing environment.


## Installation

From source:

```
git clone https://bitbucket.org/pressdb/fuzzyjoin.git
cd fuzzyjoin
python setup.py install
```

## Examples

### Self-Join

```
from pyspark import SparkContext
from pyspark.sql import SparkSession
import fuzzyjoin.self_join as self_join

# Start Spark Session
sc = SparkContext()
spark = SparkSession.builder.getOrCreate()

df_similar_pairs = self_join(spark=spark, df_filepath="data/dblp-small/raw-000/part_00000", 
                             join_columns=['publication_title','authors'], sep=":", 
                             sim_threshold=0.6, record_data=None, 
                             schema='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_id='dblp_id')

df_similar_pairs.show()
```

### R-S Join

```
from pyspark import SparkContext
from pyspark.sql import SparkSession
import fuzzyjoin.RS_join as RS_join

# Start Spark Session
sc = SparkContext()
spark = SparkSession.builder.getOrCreate()

df_similar_pairs = RS_join(spark=spark, df_R_filepath='data/pub-small/raw.dblp-000/part-00000', 
                     join_columns_R=['publication_title','authors'], sep_R=':', 
                     schema_R='dblp_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_R="2,3", record_id_R="dblp_id",
                     sim_threshold=0.5, 
                     df_S_filepath='data/pub-small/raw.csx-000/part-00000', 
                     join_columns_S=['publication_title','authors'], sep_S=':', 
                     schema_S='csx_id STRING, publication_title STRING, authors STRING, other_info STRING',
                     record_data_S="2,3", record_id_S="csx_id")

df_similar_pairs.show()
```

## Benchmarks

### R-S Join

| Dataset                                                 | Precision | Recall | F1-Measure | Jaccard Similarity Threshold |
|---------------------------------------------------------|-----------|--------|------------|------------------------------|
| [DBLP-ACM](https://dbs.uni-leipzig.de/file/DBLP-ACM.zip)| 91.6%     | 95.5%  | 93.5%      | 0.6                          |

### Self-Join

| Dataset                                                                              | Adjusted Rand Index | Jaccard Similarity Threshold |
|--------------------------------------------------------------------------------------|---------------------|------------------------------|
| [Geographic Settlements](https://dbs.uni-leipzig.de/file/geographicalSettelments.zip)| 0.55                | 0.6                          |

## Inspiration

- [Original paper by Vernica et al.](https://flamingo.ics.uci.edu/pub/sigmod10-vernica-long.pdf)
- [Original source code](https://asterix.ics.uci.edu/fuzzyjoin/fuzzyjoin-0.0.2-patch-2011-11-09.tgz)
